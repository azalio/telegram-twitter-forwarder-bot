FROM python:3.5
COPY ./requirements.txt /root/
RUN pip --no-cache-dir install -r /root/requirements.txt
RUN mkdir /app
COPY bot.py commands.py job.py main.py models.py util.py /app/
WORKDIR /app
ENTRYPOINT ["python", "main.py"]
